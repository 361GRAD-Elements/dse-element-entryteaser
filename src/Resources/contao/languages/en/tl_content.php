<?php

/**
 * 361GRAD Element Entryteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_entryteaser'] = ['Entry teaser', 'Entry teaser.'];

$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Background Color', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Background Image', ''];
$GLOBALS['TL_LANG']['tl_content']['entryteaser_legend']   = 'Teaser settings';
