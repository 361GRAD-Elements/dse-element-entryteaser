<?php

/**
 * 361GRAD Element Entryteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_entryteaser'] = ['Einstieg Elemente', 'Einstieg Elemente.'];

$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']   = ['Hintergrund Farbe', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Hintergrund Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['entryteaser_legend']   = 'Teasereinstellungen';
