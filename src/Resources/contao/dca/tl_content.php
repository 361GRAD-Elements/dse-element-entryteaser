<?php

/**
 * 361GRAD Element Entryteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_entryteaser'] =
    '{type_legend},type,headline,dse_subheadline;' .
    '{entryteaser_legend},dse_ctaHref,dse_bgcolor,dse_bgImage;' .
    '{invisible_legend:hide},invisible,start,stop';

// ELement fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class' => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgcolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        // #373F43 anthrazit
        'anthracite',
        // #D3D3D3 hellgrau
        'lightgrey',
        // #FFFFFF weiß
        'white',
        // #155579 blau
        'orange',
    ],
    'reference' => [
        'none'       => Dse\ElementsBundle\ElementEntryteaser\Element\ContentDseEntryteaser::refColor(
            '#FFFFFF',
            '#000',
            'Keine'
        ),
        'anthracite' => Dse\ElementsBundle\ElementEntryteaser\Element\ContentDseEntryteaser::refColor(
            '#373F43',
            '#FFF',
            'Anthrazit'
        ),
        'lightgrey'  => Dse\ElementsBundle\ElementEntryteaser\Element\ContentDseEntryteaser::refColor(
            '#D3D3D3',
            '#000',
            'Hellgrau'
        ),
        'white'      => Dse\ElementsBundle\ElementEntryteaser\Element\ContentDseEntryteaser::refColor(
            '#FFFFFF',
            '#000',
            'Weiß'
        ),
        'orange'     => Dse\ElementsBundle\ElementEntryteaser\Element\ContentDseEntryteaser::refColor(
            '#155579',
            '#FFF',
            'Blau'
        ),
    ],
    'eval' => [
        'tl_class' => 'clr',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class' => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
