<?php

/**
 * 361GRAD Element Entryteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_entryteaser'] =
    'Dse\\ElementsBundle\\ElementEntryteaser\\Element\\ContentDseEntryteaser';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_entryteaser'] = 'bundles/dseelemententryteaser/css/fe_ce_dse_entryteaser.css';

