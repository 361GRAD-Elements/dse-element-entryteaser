<?php

namespace Dse\ElementsBundle\ElementEntryteaser\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementEntryteaser;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementEntryteaser\DseElementEntryteaser::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
